import os

word = ''
old_letter = []
fail = 0
result = None

def clear_screen():
    os.system('cls' if os.name == 'nt' else 'clear')

def set_word():
    word = input("Podaj słowo: ")
    current_word = len(word) * '_'
    print(current_word)
    clear_screen()
    return word


def check(letter):
    global fail

    if len(letter) > 1:
        fail += 1
        return print("Tylko jeden znak!")

    if (letter in word):
        print("JEST!!!")
    else:
        print("Nie ma takiej litery")
        fail += 1



    print_gallows(fail)

    if fail == 6:
        return 0

    if print_current_word() == 1:
        return 1

def print_current_word():
    current_word = ""
    for x in word:
        if x in old_letter:
           current_word += x
        else:
            current_word += '_'
    print(f'HASŁO DO ODGADNIĘCIA: {current_word}')
    if current_word == word:
        return 1


def print_old_letter():
    old = ""
    for x in old_letter:
        old = old + " " + x
    print(f"Wybrane litery to: {old}")

def print_gallows(fail):
    if fail == 1:
        print("""
        
        
        
        /
        """)

    elif fail == 2:
        print("""



        /\\
              """)

    elif fail == 3:
        print("""
         |
         |
         |
         |
        / \\
              """)
    elif fail == 4:
        print("""
          ______         
         |
         |
         |
         |
        / \\
              """)
    elif fail == 5:
        print("""
          ______         
         |      |
         |
         |
         |
        / \\
                 """)
    elif fail == 6:
        print("""
          ______         
         |      |
         |      0
         |     /|\\
         |     / \\
        / \\
                 """)



word = set_word()

while (True):
    print(f"Błędy: {fail}")
    print_old_letter()
    letter = input("Podaj literę: ")
    old_letter.append(letter)
    result = check(letter)
    if result == 0:
        print("KONIEC GRY PRZEGRANA")
        break;
    elif result == 1:
        print("KONIEC GRY - Gratulacje ")
        break;
    print("*" * 100)




